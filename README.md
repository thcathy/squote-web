# Squote-web #

This is the interface of my personal website using Angular 2 (Typescript)

### How to build ###

```
npm install
npm run gulp build
```

#### Run development env with browser sync ####
```npm run gulp serve```

#### Build Production env ####
```NODE_ENV=production npm run gulp build```

### Configuration ###

Configuration are stored in config/config.json or config-prd.json

### Note ###

This is a personal project by [Timmy Wong](https://github.com/thcathy).
